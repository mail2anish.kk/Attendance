"use strict";
var GoogleSpreadsheet = require("google-spreadsheet");
var dbUtil = require("../../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;
var moment = require('moment-timezone');

exports.getClassStudents = function(req, res, next) {
    if (req.body && req.body.sheetId) {

        console.log(req.body.sheetId);
        var my_sheet = new GoogleSpreadsheet(req.body.sheetId);

        try {
            my_sheet.getRows(1, function(err, row_data) {
                var studentList = [];

                for (var i = 0; i < row_data.length; i++) {
                    var stuInfo = {
                        "fname": row_data[i].firstname,
                        "lname": row_data[i].lastname,
                        "rollno": row_data[i].rollno,
                        "email": row_data[i].email,
                        "phone": row_data[i].phn,
                    }
                    studentList.push(stuInfo)
                    //console.log(JSON.stringify(row_data[i].firstname));
                }

                //console.log( 'pulled in '+row_data.length + ' rows');
                res.json({
                    "studentsList": studentList
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}

exports.timezoneapi = function(req, res, next) {
    var today = new Date();
    var date = moment().utcOffset("+05:30").format();
    /* date will a string like-  2016-07-08T18:09:18+05:30   */
    
    console.log(date);
    /* get year,month,day,hours,minutes from date string */
    var yyyy = date.substring(0, date.indexOf("-"));
    var mm = date.substring(date.indexOf("-") + 1, date.lastIndexOf("-"));
    var dd = date.substring(date.lastIndexOf("-") + 1, date.indexOf("T"));
    var hh = date.split('T')[1].split(':')[0];
    var minutes = date.split('T')[1].split(':')[1];
    var sec = date.split('T')[1].split(':')[2].substr(0, 2);
    //return new Date(yyyy, mm - 1, dd, hh, minutes, sec, today.getMilliseconds());
    var t = new Date(yyyy, mm - 1, dd, hh, minutes, sec, today.getMilliseconds());
    console.log(t+" "+date);

}