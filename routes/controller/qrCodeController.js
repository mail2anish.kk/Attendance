"use strict";
var dbUtil = require("../../config/dbUtil");
var ObjectId = require('mongodb').ObjectID;

exports.addQrCode = function(req, res, next) {
    if (req.body && req.body.qrCode && req.body.qrName && req.body.tenant) {

        var qrCode = req.body.qrCode
        var qrName = req.body.qrName
        var tenant = req.body.tenant
        try {
            var data = {
                'qrCode': qrCode,
                'qrName': qrName,
            };
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_QrCodes";
                db.collection(tableName).find({
                    "qrCode": qrCode
                }).toArray(function(err, result) {
                    if(result.length > 0) {
                        res.json({
                                "error": "Qr code already exist"
                            });
                    }else{
                    db.collection(tableName).insertOne(data, function(err, result3) {
                        if (!err) {
                            res.json({
                                "success": "Qr code added"
                            });
                        }
                    });
                }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}


exports.getQrCode = function(req, res, next) {
    if (req.body && req.body.tenant) {
        var tenant = req.body.tenant
        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_QrCodes";
                db.collection(tableName).find({
                }).toArray(function(err, result) {
                    if(result.length > 0) {
                        res.json({'QrCodes':result});
                    }else{
                            res.json({
                                "error": "Qr codes not available. Please add."
                            });
                    }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}


exports.getAvailableQrCodes = function(req, res, next) {
    if (req.body && req.body.tenant) {
        var tenant = req.body.tenant;
        var availableQrCodes = [];
        try {
            dbUtil.getConnection(function(db) {
                var tableName = "T_" + tenant + "_QrCodes";
                db.collection(tableName).find({
                }).toArray(function(err, qrCodes) {
                    if(qrCodes.length > 0) {
                        var tableName = "T_" + tenant + "_activeQrCodes";
                        db.collection(tableName).find({
                            }).toArray(function(err, activeCodes) {
                                for(var i=0;i<qrCodes.length;i++){
                                    var found=false;
                                    for(var j=0;j<activeCodes.length;j++){
                                        if(qrCodes[i].qrCode == activeCodes[j].qrCode){
                                            found = true;
                                        }
                                    }
                                    if(!found){ availableQrCodes.push({"qrCode":qrCodes[i].qrCode,"qrName":qrCodes[i].qrName})  }
                                }
                                res.json({'QrCodes':availableQrCodes});
                            });
                    
                    }else{
                            res.json({
                                "error": "Qr codes not available. Please add."
                            });
                    }
                });
            });
        } catch (e) {
            res.json({
                "error": "Some error occurred. Please try again."
            });
        }
    } else {
        res.status(401).json({
            "error": "Parameters missing"
        });
    }
}

